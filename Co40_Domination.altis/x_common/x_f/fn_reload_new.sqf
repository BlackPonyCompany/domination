// by Xeno
#define __DEBUG__
#define THIS_FILE "fn_reload.sqf"
#include "x_setup.sqf"
private ["_magazines","_object","_type","_type_name"];

#define __NOALIEX if (!alive _object) exitWith {};

_object = [_this, 0] call BIS_fnc_param;
__TRACE_1("","_object")
if (typeName _object == "ARRAY") then {
	_obb = objNull;
	{
		if ((_x isKindOf "Helicopter" || {_x isKindOf "LandVehicle"} || {_x isKindOf "Plane"}) && {!(_x isKindOf "HeliH")} && {!(_x isKindOf "Land_HelipadSquare_F")}) exitWith {
			_obb = _x;
		};
	} forEach _object;
	_object = _obb;
};

if (isNull _object || {_object isKindOf "ParachuteBase"}) exitWith {};

_type = typeOf _object;
__TRACE_2("","_object","_type")

_lrl = _object getVariable ["d_last_reload", -1];
if (_lrl != -1 && {(time - _lrl) < 60}) exitWith {
	if (!isDedicated) then {
		_object vehicleChat format [localize "STR_DOM_MISSIONSTRING_699", round (60 - (time - _lrl))];
	};
};
_object setVariable ["d_last_reload", time];

if (isNil "d_reload_time_factor") then {d_reload_time_factor = 1};

__NOALIEX
_object setFuel 0;

_type_name = [_type, "CfgVehicles"] call d_fnc_GetDisplayName;
if (_type_name == "") then {_type_name = _type};
if (!isDedicated) then {_object vehicleChat format [localize "STR_DOM_MISSIONSTRING_701", _type_name]};


private "_allturrets";
_allturrets = allTurrets [_object, false];
_allturrets pushBack [-1];
__TRACE_1("","_allturrets")
if (count _allturrets > 0) then {
	_turretsmags = [];
	{
		_magazines = _object magazinesTurret _x;
		_turretsmags pushBack [_x, _magazines];
		__TRACE_1("","_magazines")
		if (_object turretLocal _x && {count _magazines > 0}) then {
			_removedX = [];
			_curturret = _x;
			{
				if !(_x in _removedX) then {
					_object removeMagazinesTurret [_x, _curturret];
					_removedX pushBack _x;
				};
				__NOALIEX
			} forEach _magazines;
			__NOALIEX
		};
	} forEach _allturrets;
	__TRACE_1("","_turretsmags")
	__NOALIEX
	{
		_curturret = _x select 0;
		_magazines = _x select 1;
		{
			private "_mag_disp_name";
			_mag_disp_name = [_x, "CfgMagazines"] call d_fnc_GetDisplayName;
			if (_mag_disp_name == "") then {_mag_disp_name = _x};
			if (!isDedicated) then {_object vehicleChat format [localize "STR_DOM_MISSIONSTRING_702", _mag_disp_name]};
			sleep d_reload_time_factor;
			__NOALIEX
			__TRACE_1("","_x")
			if (_object turretLocal _curturret) then {
				if (_x == "SmokeLauncherMag") then {
					_object removeWeaponTurret ["SmokeLauncher", _curturret];
				};
				_object addMagazineTurret [_x, _curturret];
				if (_x == "SmokeLauncherMag") then {
					_object addWeaponTurret ["SmokeLauncher", _curturret];
				};
			};
			sleep d_reload_time_factor;
			__NOALIEX
		} forEach _magazines;
		__NOALIEX
	} forEach _turretsmags;
};

__NOALIEX

_magazines = getArray(configFile >> "CfgVehicles" >> _type >> "magazines");
__TRACE_1("","_magazines");

if (count _magazines > 0) then {
	_removed = [];
	{
		if !(_x in _removed) then {
			_object removeMagazines _x;
			_removed pushBack _x;
			__TRACE_1("remMag","_x");
		};
	} forEach _magazines;
	__TRACE_1("","_removed");
	{
		if (!isDedicated) then {_object vehicleChat format [localize "STR_DOM_MISSIONSTRING_702", _x]};
		sleep d_reload_time_factor;
		if (!alive _object) exitWith {};
		_object addMagazine _x;
		__TRACE_1("addMag","_x");
	} forEach _magazines;
};
_object setVehicleAmmo 1;

__NOALIEX

sleep d_reload_time_factor;
__NOALIEX
if (!isDedicated) then {_object vehicleChat (localize "STR_DOM_MISSIONSTRING_704")};
_object setDamage 0;
sleep d_reload_time_factor;
__NOALIEX
if (!isDedicated) then {_object vehicleChat (localize "STR_DOM_MISSIONSTRING_705")};
while {fuel _object < 0.99} do {
	_object setFuel 1;
	sleep 0.01;
};
sleep d_reload_time_factor;
__NOALIEX
if (!isDedicated) then {_object vehicleChat format [localize "STR_DOM_MISSIONSTRING_706", _type_name]};

reload _object;