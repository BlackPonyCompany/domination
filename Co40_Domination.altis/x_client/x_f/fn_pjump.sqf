#define THIS_FILE "fn_pjump.sqf"
#include "x_setup.sqf"

if (isDedicated) exitWith {};

private ["_startLocation","_jumphelo","_obj_jump"];

_startLocation = [_this, 0] call BIS_fnc_param;

if (d_HALOWaitTime > 0) then {d_next_jump_time = time + d_HALOWaitTime};

titleText ["","Plain"];
_jumphelo = createVehicle [d_jump_helo, _startLocation, [], 0, "FLY"];
_jumphelo setPos _startLocation;
_jumphelo engineOn true;
_obj_jump = player;
_obj_jump moveInCargo _jumphelo;
if (vehicle player == player) exitWith {};

_obj_jump setVelocity [0,0,0];
_obj_jump action["eject", vehicle _obj_jump];

sleep 3;

deleteVehicle _jumphelo;
if (d_with_ai && {alive player} && {!(player getVariable ["xr_pluncon", false])}) then {[getPosATL player, velocity player, getDirVisual player] spawn d_fnc_moveai};