#define THIS_FILE "fn_paraj.sqf"
#include "x_setup.sqf"

if (isDedicated) exitWith {};

private ["_do_exit","_realpos", "_jumpobj", "_add_pchute_bp"];

_jumpobj = [_this, 0] call BIS_fnc_param;

if (player distance _jumpobj > 15) exitWith {};

_do_exit = false;
if (d_with_ranked) then {
	if (score player < (d_ranked_a select 4)) then {
		[playerSide, "HQ"] sideChat format [localize "STR_DOM_MISSIONSTRING_64", score player, d_ranked_a select 4];
		_do_exit = true;
	} else {
		["d_pas", [player, (d_ranked_a select 4) * -1]] call d_fnc_NetCallEventCTS;
	};
};

if (_do_exit) exitWith {};

if (isNil "d_next_jump_time") then {d_next_jump_time = -1};

if (d_HALOWaitTime > 0 && {player distance d_FLAG_BASE < 15} && {d_next_jump_time > time}) exitWith {
	[playerSide, "HQ"] sideChat format [localize "STR_DOM_MISSIONSTRING_65", round ((d_next_jump_time - time)/60)];
};

_bpbp = backpack player;
_add_pchute_bp = false;
if (_bpbp == "") then {
	_add_pchute_bp = true;
} else {
	if (_bpbp != "" && {_bpbp != "B_Parachute"}) then {
		//[playerSide, "HQ"] sideChat (localize "STR_DOM_MISSIONSTRING_66");
		_do_exit = true;
	};
};
//if (_do_exit) exitWith {};

d_global_jump_pos = [];
createDialog "d_ParajumpDialog";

waitUntil {!d_parajump_dialog_open || {!alive player} || {player getVariable ["xr_pluncon", false]}};
if (alive player && {!(player getVariable ["xr_pluncon", false])}) then {
	if !(d_global_jump_pos isEqualTo []) then {
		if (_do_exit) then {
			if (!isNull (unitBackpack player)) then {
				private ["_pack","_class","_magazines","_weapons","_items","_helmet"];
				_pack	   = unitBackpack player;
				_class	   = typeOf _pack;
				_magazines = getMagazineCargo _pack;
				_weapons   = getWeaponCargo _pack;
				_items	   = getItemCargo _pack;

				removeBackpack player; //remove the backpack
				_add_pchute_bp = true; //add the parachute

				[player,_class,_magazines,_weapons,_items] spawn {
					private ["_unit","_class","_magazines","_weapons","_items","_helmet"];
					_unit		= _this select 0;
					_class		= _this select 1;
					_magazines	= _this select 2;
					_weapons 	= _this select 3;
					_items 		= _this select 4;

					private "_packHolder";
					_packHolder = createVehicle ["groundWeaponHolder", [0,0,0], [], 0, "can_collide"];
					_packHolder addBackpackCargoGlobal [_class, 1];

					waitUntil {animationState _unit == "HaloFreeFall_non"};
					_packHolder attachTo [_unit,[-0.12,-0.02,-.74],"pelvis"];
					_packHolder setVectorDirAndUp [[0,-1,-0.05],[0,0,-1]];

					waitUntil {animationState _unit == "para_pilot"};
					_packHolder attachTo [vehicle _unit,[-0.07,0.67,-0.13],"pelvis"];
					_packHolder setVectorDirAndUp [[0,-0.2,-1],[0,1,0]];

					waitUntil {isTouchingGround _unit || (getPos _unit select 2) < 1};
					detach _packHolder;
					deleteVehicle _packHolder; //delete the backpack in front

					_unit addBackpack _class; //return the backpack
					clearAllItemsFromBackpack _unit; //clear all gear from new backpack

					for "_i" from 0 to (count (_magazines select 0) - 1) do {
						(unitBackpack _unit) addMagazineCargoGlobal [(_magazines select 0) select _i,(_magazines select 1) select _i]; //return the magazines
					};
					for "_i" from 0 to (count (_weapons select 0) - 1) do {
						(unitBackpack _unit) addWeaponCargoGlobal [(_weapons select 0) select _i,(_weapons select 1) select _i]; //return the weapons
					};
					for "_i" from 0 to (count (_items select 0) - 1) do {
						(unitBackpack _unit) addItemCargoGlobal [(_items select 0) select _i,(_items select 1) select _i]; //return the items
					};
				};
			};
		};

		if (_add_pchute_bp) then {
			player addBackpack "B_Parachute";
		};
		_realpos = [d_global_jump_pos, 200, d_HALOJumpHeight] call d_fnc_GetRanJumpPoint;
		[_realpos] spawn d_fnc_pjump;
	};
} else {
	if (d_parajump_dialog_open) then {closeDialog 0};
};
